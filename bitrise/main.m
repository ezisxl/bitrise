//
//  main.m
//  bitrise
//
//  Created by Ansis Lasmanis on 12/02/16.
//  Copyright © 2016 AppLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
