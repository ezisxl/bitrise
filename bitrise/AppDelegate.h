//
//  AppDelegate.h
//  bitrise
//
//  Created by Ansis Lasmanis on 12/02/16.
//  Copyright © 2016 AppLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

